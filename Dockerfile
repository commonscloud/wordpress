ARG TAG=php8.3-apache

FROM wordpress:$TAG

RUN apt update && apt dist-upgrade -y

RUN apt install -y libldap-dev libldap-common \
	&& docker-php-ext-configure ldap \
	&& docker-php-ext-install ldap

RUN pecl install redis \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable redis

RUN apt install -y ca-certificates
