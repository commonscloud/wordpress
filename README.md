# Wordpress

Wordpress image PHP 8.3 / Apache2 expanded with:

* ldap php modules
* redis php modules
* ca-certificates


## Build

```
docker build . -t registry.commonscloud.coop/wordpress --build-arg TAG=latest
```

